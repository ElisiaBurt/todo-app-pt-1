import React, { Component } from "react";
import todosList from "./todos.json";
import uuid from 'react-uuid'

class App extends Component {
  state = {
    todos: todosList,
  };



  handleToggle = (id) => {
    const todos = [...this.state.todos]; //copy of todos
    const todoIndex = todos.findIndex(currentTodo => currentTodo.id === id)
    todos[todoIndex].completed = !todos[todoIndex].completed //the logic that is toggling
    
    this.setState({ todos }) //replace the state with the changed copy of array
  }

  handleDelete = (id) => {

    const newTodo = this.state.todos.filter(
      todoItems => todoItems.id !== id
    )
    this.setState({ todos: newTodo })
  }

  handleDeleteComplete = () => {

    const newTodo = this.state.todos.filter(
      todoItems => todoItems.completed !== true
    )
    this.setState({ todos: newTodo })
  }

  handleAddTo = (event) => {
    if (event.key === "Enter") {
      const addItems = {
        userId: 1,
        id: uuid(),
        // id: Math.floor(Math.random()*50),
        title: event.target.value,
        completed: false
      }
      const newTodo = this.setState({
        todos: [...this.state.todos, addItems]
       
      })
      event.target.value = ""
    }
  }



  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" autoFocus onKeyDown={this.handleAddTo}/>
        </header>
        <TodoList
          todos={this.state.todos}
          handleToggle={this.handleToggle}
          handleDelete={this.handleDelete} />
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.length}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleDeleteComplete}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              // key={todo.id}
              id={todo.id}
              title={todo.title}
              completed={todo.completed}
              handleToggle={this.props.handleToggle}
              handleDelete={this.props.handleDelete}
            />
          ))}
        </ul>
      </section>
    );
  }
}

class TodoItem extends Component {


  render() {

    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox"
            checked={this.props.completed}
            onChange={this.props.handleToggle.bind(this, this.props.id)} />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={() => this.props.handleDelete(this.props.id)} />
        </div>
      </li>
    );
  }
}



export default App;
